package fpluquet.be.quizz.views;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import fpluquet.be.quizz.R;
import fpluquet.be.quizz.db.GameDbHelper;
import fpluquet.be.quizz.models.QuestionModel;

public class MainActivity extends AppCompatActivity {

    private static final int QUESTION_RESULT = 1;
    private static final String CURRENT_QUESTION_INDEX = "be.fpluquet.android.CURRENT_QUESTION_INDEX";
    private static final String SAVING_FILENAME = "data.txt";
    private ArrayList<QuestionModel> questions;
    private int currentQuestionIndex = 0;
    private int score = 0;
    private GameDbHelper mDbHelper;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.cleanHistory :
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Patientez...");

                progressDialog.setMax(100);
                progressDialog.setProgress(0);
                progressDialog.setIndeterminate(false);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int i = 0;
                        while(i < 11){
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            progressDialog.setProgress(i * 10);
                            i++;
                        }
                        progressDialog.dismiss();
                    }
                }).start();

                Toast.makeText(this, "Clean history", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.showHistory :
                Toast.makeText(this, "Show history", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setQuestions();
        mDbHelper = new GameDbHelper(this);

        if(savedInstanceState != null) {
            currentQuestionIndex = savedInstanceState.getInt(CURRENT_QUESTION_INDEX);
            Toast.makeText(this, "CURRENT : " + currentQuestionIndex, Toast.LENGTH_SHORT).show();
        }
        try {
            FileInputStream file = openFileInput(SAVING_FILENAME);
            BufferedReader input = new BufferedReader(new InputStreamReader(file));
            currentQuestionIndex = Integer.parseInt(input.readLine());
            score = Integer.parseInt(input.readLine());
            findViewById(R.id.currentGamePane).setVisibility(View.VISIBLE);
            updateScoreView();
            file.close();
        } catch (Exception e) {
            findViewById(R.id.currentGamePane).setVisibility(View.INVISIBLE);
        }
    }

    private void setQuestions() {
        questions = new ArrayList<>();

        InputStream inputStream = this.getResources().openRawResource(R.raw.questions);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader bufferedreader = new BufferedReader(inputreader);
        String line;
        QuestionModel questionModel = null;
        try
        {
            while (( line = bufferedreader.readLine()) != null)
            {
                if(line.length() != 0) {
                    if(questionModel == null) {
                        questionModel = new QuestionModel();
                        questions.add(questionModel);
                        questionModel.setText(line);
                    }else {
                        questionModel.addAnswer(line);
                    }
                } else {
                    questionModel = null;
                }
            }
        }
        catch (IOException e)
        {
        }

        // initialize the current question index
        currentQuestionIndex = 0;
    }
    private void updateScoreView(){
        ((TextView)findViewById(R.id.scoreTV)).setText("" + this.score);
    }
    private void launchNextQuestion() {
        saveCurrentGameState();

        QuestionModel question = questions.get(currentQuestionIndex);
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(QuestionActivity.QUESTION, question);

        startActivityForResult(intent, QUESTION_RESULT);
    }

    private void saveCurrentGameState() {
        try {
            FileOutputStream outputStream = openFileOutput(SAVING_FILENAME, MODE_PRIVATE);
            outputStream.write((currentQuestionIndex + "\n").getBytes());
            outputStream.write((score + "\n").getBytes());
            outputStream.close();
            findViewById(R.id.currentGamePane).setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == QUESTION_RESULT && resultCode == RESULT_OK) {
            boolean isCorrect = data.getBooleanExtra(QuestionActivity.IS_CORRECT, false);
            String toShow;
            if(isCorrect){
                toShow = "Bravo !";
                score ++;
                updateScoreView();
            } else {
                toShow = "Perdu :(";
            }
            Toast.makeText(this, toShow, Toast.LENGTH_SHORT).show();
            currentQuestionIndex ++;
            if(currentQuestionIndex >= questions.size()) {
                findViewById(R.id.currentGamePane).setVisibility(View.INVISIBLE);
                deleteFile(SAVING_FILENAME);
                mDbHelper.insertGame(score);

                Toast.makeText(this, "Plus de questions...", Toast.LENGTH_SHORT).show();
                return;
            }
            launchNextQuestion();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_QUESTION_INDEX, currentQuestionIndex);
        super.onSaveInstanceState(outState);
    }

    public void startNewGame(View view){
        currentQuestionIndex = 0;
        score = 0;
        updateScoreView();
        findViewById(R.id.currentGamePane).setVisibility(View.INVISIBLE);
        launchNextQuestion();
    }

    public void continueGame(View view){
        launchNextQuestion();
    }


}
