package fpluquet.be.quizz.views;

import android.content.Intent;
import android.graphics.Color;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fpluquet.be.quizz.R;
import fpluquet.be.quizz.exceptions.InternalGameException;
import fpluquet.be.quizz.models.QuestionModel;


public class QuestionActivity extends AppCompatActivity {

    public static final String QUESTION = "be.helha.android.QUESTION";
    public static final String IS_CORRECT = "be.helha.android.IS_CORRECT";
    private static final String SELECTED_ANSWER = "be.helha.android.SELECTED_ANSWER";
    private static final String ANSWERS = "be.helha.android.ANSWERS";

    private QuestionModel question;
    private ArrayList<String> answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        question = (QuestionModel) getIntent().getSerializableExtra(QUESTION);

        if(question == null){
            throw new InternalGameException("No question passed to QuestionModel activity");
        }
        String selectedAnswer = null;
        if(savedInstanceState == null) {
            answers = question.getRandomizedAnswers();
        }else {
            answers = savedInstanceState.getStringArrayList(ANSWERS);
            selectedAnswer = savedInstanceState.getString(SELECTED_ANSWER);
        }

        setQuestion(question, selectedAnswer);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        answers = savedInstanceState.getStringArrayList(ANSWERS);
        String selectedAnswer = savedInstanceState.getString(SELECTED_ANSWER);
        setQuestion(question, selectedAnswer);
    }

    private void setQuestion(QuestionModel question, String preselectedAnswer) {

        TextView questionTV = (TextView) findViewById(R.id.question);
        questionTV.setText(question.getText());

        RadioGroup radioGroup = getRadioGroup();
        radioGroup.removeAllViews();
        for(String answer: answers){
            RadioButton rb = new RadioButton(this);
            rb.setText(answer);
            rb.setPadding(0,0,0, 20);
            if(question.isCorrectAnswer(answer))
                rb.setBackgroundColor(Color.GRAY);
            rb.setTextAppearance(this, android.R.style.TextAppearance_Large);
            radioGroup.addView(rb);
            if (answer.equals(preselectedAnswer)) {
                rb.toggle();
            }
        }
    }

    private RadioGroup getRadioGroup() {
        return (RadioGroup) findViewById(R.id.answersRG);
    }

    public void onValidateClick(View view){
        String selectedAnswer = getSelectedAnswer();
        if(selectedAnswer == null){
            Toast.makeText(this, "Vous devez sélectionner une réponse", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent();
        boolean isCorrect = question.isCorrectAnswer(selectedAnswer);
        intent.putExtra(IS_CORRECT, isCorrect);
        setResult(RESULT_OK, intent);
        finish();
    }

    private RadioButton getSelectedRadioButton() {
        return (RadioButton) findViewById(getRadioGroup().getCheckedRadioButtonId());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(SELECTED_ANSWER, getSelectedAnswer());
        outState.putStringArrayList(ANSWERS, answers);
        super.onSaveInstanceState(outState);
    }

    private String getSelectedAnswer() {
        RadioButton radioButton = getSelectedRadioButton();
        if(radioButton == null){
            return null;
        }
        return radioButton.getText().toString();
    }
}
