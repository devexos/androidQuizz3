package fpluquet.be.quizz.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by fpluquet on 10/11/17.
 */

public class GameDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Game.db";

    /* Inner class that defines the table contents */
    public static class GameEntry implements BaseColumns {
        public static final String TABLE_NAME = "Game";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_SCORE = "score";
    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + GameEntry.TABLE_NAME + " (" +
                    GameEntry._ID + " INTEGER PRIMARY KEY," +
                    GameEntry.COLUMN_NAME_DATE + " INT," +
                    GameEntry.COLUMN_NAME_SCORE + " INT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + GameEntry.TABLE_NAME;

    public GameDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public void insertGame(int score){
        SQLiteDatabase db = this.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(GameEntry.COLUMN_NAME_DATE, System.currentTimeMillis());
        values.put(GameEntry.COLUMN_NAME_SCORE, score);

        // Insert the new row, returning the primary key value of the new row
        db.insert("Game", null, values);
    }
}
